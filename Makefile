build:
	sudo docker build --force-rm $(options) -t dumnas-owncloud:latest .

build-prod:
	$(MAKE) build options="--target production"

compose-start:
	sudo docker-compose up --remove-orphans $(options)

compose-stop:
	sudo docker-compose down --remove-orphans $(options)

compose-config:
	sudo docker-compose config $(options)